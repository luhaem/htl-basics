
public class Battery {
	private int chargingStatus;

	public Battery(int chargingStatus) {
		super();
		this.chargingStatus = chargingStatus;
	}

	public int getChargingStatus() {
		return chargingStatus;
	}

	public void setChargingStatus(int chargingStatus) {
		if(chargingStatus >= 0 && chargingStatus <= 100) {
		this.chargingStatus = chargingStatus;
		}
	}
	
}
