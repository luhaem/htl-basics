
public class Remote {

	private boolean isOn;
	private boolean hasPower;
	private Battery battery1;
	private Battery battery2;
	
	public Remote(boolean isOn, Battery battery1, Battery battery2) {
		super();
		this.isOn = isOn;
		this.battery1 = battery1;
		this.battery2 = battery2;
	}

	public boolean isOn() {
		return isOn;
	}
	
	public boolean HasPower() {
		if(getStatus()< 15) {
			hasPower = false;
		}
		else hasPower = true;
		return hasPower;
	}

	public void turnOn() {
		isOn = true;
	}
	
	public void turnOff() {
		isOn = false;
	}
	
	public int getStatus() {
		int status = (battery1.getChargingStatus() + battery2.getChargingStatus())/2;
		return status;
	}
}
