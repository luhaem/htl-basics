
public class Main {
	
	
	public static void main(String[] args) {
		
		Battery battery1 = new Battery(100);
		Battery battery2 = new Battery(90);
		//Remote aus
		Remote remote = new Remote(false, battery1, battery2);
		
		System.out.println(remote.isOn());
		System.out.println(remote.getStatus() + "%");
		//Battery ein
		remote.turnOn();
		System.out.println(remote.isOn());
		
	}
	
}
