
public class Engine {

	private String propulsionMethod;
	private int power;
	
	public Engine(String propulsionMethod, int power) {
		super();
		this.propulsionMethod = propulsionMethod;
		this.power = power;
	}

	public String getPropulsionMethod() {
		return propulsionMethod;
	}

	public void setPropulsionMethod(String propulsionMethod) {
		this.propulsionMethod = propulsionMethod;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}
}
