import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.time.LocalDate;
import java.time.Period;

public class Person {

	private String firstName, lastName;
	private int age;
	private List<Car> cars;
	private LocalDate birthDate;

	public Person(String firstName, String lastName, LocalDate birthDate) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.cars = new ArrayList<>();
	}
	
	public void addCar(Car car) {
		this.cars.add(car);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void printCars() {
		for (Car car : cars) {
			System.out.println(car.getColor());
		}
	}
	
	public int getAge() {
		    // validate inputs ...
			LocalDate currentDate = LocalDate.now();
		    return Period.between(birthDate, currentDate).getYears();
	}
	
	public void printCarsValue() {
		double price = 0;
		for (Car car : cars) {
			price = price + car.getPrice();
		}
		System.out.println(price);
	}
	
}
