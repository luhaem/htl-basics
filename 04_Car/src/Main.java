import java.time.LocalDate;

public class Main {
	
	public static void main(String[] args) {
		
		Engine engine1 = new Engine("Diesel", 150);
		Engine engine2 = new Engine("Benzin", 200);
		Engine engine3 = new Engine("Benzin", 310);
		
		Manufacturer manufacturer1 = new Manufacturer("VW", "germany", 25);
		Manufacturer manufacturer2 = new Manufacturer("Opel", "germany", 10);
		Manufacturer manufacturer3 = new Manufacturer("Ford", "USA", 30);
		
		Car car1 = new Car("blue", 220, 50000, 7, engine1, manufacturer1);
		Car car2 = new Car("green", 200, 20000, 4, engine2, manufacturer2);
		Car car3 = new Car("red", 250, 70000, 8, engine3, manufacturer3);
		
		Person person1 = new Person("Luca", "Blaser", LocalDate.of(2000,9,25));
		Person person2 = new Person ("Can", "Bas", LocalDate.of(1999,4,21));
		
		person1.addCar(car1);
		person1.addCar(car2);
		
		person1.printCars();
		
		person1.printCarsValue();

		System.out.println("Alter von: " + person1.getFirstName() + " " + person1.getAge());
	}
}
