
public class Car {
	private String color;
	private int maxSpeed;
	private double basePrice;
	private double baseConsumtion;
	private Engine engine;
	private Manufacturer manufacturer;
	private int mileage;
	
	public Car(String color, int maxSpeed, double basePrice, double baseConsumtion, Engine engine, Manufacturer manufacturer) {
		super();
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basePrice = basePrice;
		this.baseConsumtion = baseConsumtion;
		this.engine = engine;
		this.manufacturer= manufacturer;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
		setBaseConsumtion(mileage);
	}

	public String getColor() {
		return color;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public double getBasePrice() {
		return basePrice;
	}
	
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}

	public double getBaseConsumtion() {
		return baseConsumtion;
	}

	public void setBaseConsumtion(double baseConsumtion) {
		double result;
		if(mileage >= 50000) {
			result = baseConsumtion * 1.098;
		}
		else {
			result = baseConsumtion;
		}
		
		this.baseConsumtion = result;
	}
	
	public double getPrice() {
		double price = basePrice* ((100-manufacturer.getDiscount()) * 0.01);
		return price;
	}
	
	public String getType() {
		return engine.getPropulsionMethod();
	}
	
	public double getUsage() {
		return baseConsumtion;
	}
}
