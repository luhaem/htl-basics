import java.util.Date;
public class Manufacturer {

	private String name;
	private String countryOfOrigin;
	private int discount;

	
	public Manufacturer(String name, String countryOfOrigin, int discount) {
		super();
		this.name = name;
		this.countryOfOrigin = countryOfOrigin;
		this.discount = discount;
	}


	public String getName() {
		return name;
	}

	public String getCountryOfOrigin() {
		return countryOfOrigin;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	
}
