package at.haem.FactoryPattern;

import java.util.ArrayList;
import java.util.List;

public class Game {

	public List<Actor> actors;

	public Game() {
		this.actors = new ArrayList<Actor>();
	}
	
	public void addActor() {
		RandomActorFactory RAF = new RandomActorFactory();
		actors.add(RAF.createRandomActor());
	}
	
	public void moveAll() {
		for (Actor actor : actors) {
			actor.move();
		}
	}
}
