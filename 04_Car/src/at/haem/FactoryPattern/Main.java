package at.haem.FactoryPattern;

public class Main {
	
	public static void main(String[] args) {
		
		RandomActorFactory RAF = new RandomActorFactory();
		Game game = new Game();

		for (int i = 0; i < 5; i++) {
			game.addActor();			
		}
		game.moveAll();	
	}
}
