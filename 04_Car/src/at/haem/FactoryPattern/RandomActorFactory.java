package at.haem.FactoryPattern;

public class RandomActorFactory {
	
	public double x;
	
	public RandomActorFactory() {

	}

	public Actor createRandomActor() {
		x = (int)(Math.random() * ((2 - 1) + 1)) + 1;  
		if(x==1) {
			Actor homer = new Homer();
			return homer;
		}
		else {
			Actor snowflake = new Snowflake();
			return snowflake;
		}	
	}
}
