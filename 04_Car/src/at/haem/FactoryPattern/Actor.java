package at.haem.FactoryPattern;

public interface Actor {
	public void move();
	public void render();
}
