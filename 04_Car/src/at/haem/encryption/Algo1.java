package at.haem.encryption;

import javax.crypto.Cipher;

import org.jasypt.util.text.AES256TextEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;

public class Algo1 extends AbstractEncryptor{

	public String founder;
	BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
	
	public Algo1(String founder) {
		super();
		this.founder = founder;
	}

	@Override
	public String encrypt(String text) {

		textEncryptor.setPassword("pw");
		String myEncryptedText = textEncryptor.encrypt(text);
		return myEncryptedText;
	}

	@Override
	public String decrypt(String text) {
		String plainText = textEncryptor.decrypt(text);
		return plainText;
	}
}
