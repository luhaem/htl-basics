package at.haem.encryption;

public interface Encrypter {
	public String encrypt(String text);
	public String decrypt(String text);
	public String getFounder(String founder);
}
