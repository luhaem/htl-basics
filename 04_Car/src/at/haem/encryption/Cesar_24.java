package at.haem.encryption;

public class Cesar_24 implements Encrypter{

	private int amount;
	
	public Cesar_24(int amount) {
		super();
		this.amount = amount;
	}

	@Override
	public String encrypt(String text) {
		
		char[] toArray = text.toCharArray();
		String returnValue = "";
		String newChar = "";
		for (int i = 0; i < toArray.length; i++) {
			int number = toArray[i];
			int toAmount = 0;
			if((int)toArray[i]>=(int)'a'&& (int)toArray[i]<=(int)'z') {
				number += amount;
				if(number-(int)'z' > 0 ) {
					number = (int)'a'-1 + number-(int)'z';
				}
				newChar = String.valueOf((char)number);
			}
			else if((int)toArray[i]>=(int)'A'&& (int)toArray[i]<=(int)'Z') {
				number += amount;
				if(number-(int)'Z' > 0 ) {
					number = (int)'A'-1 + number-(int)'Z';
				}
				newChar = String.valueOf((char)number);				
			}
			else {
				newChar = ""+ toArray[i];
			}
			
			returnValue = returnValue + newChar;
		}
		return returnValue;
	}

	@Override
	public String decrypt(String text) {
		
		char[] toArray = text.toCharArray();
		String returnValue = "";
		String newChar = "";
		for (int i = 0; i < toArray.length; i++) {
			int number = toArray[i];
			int toAmount = 0;
			if((int)toArray[i]>=(int)'a'&& (int)toArray[i]<=(int)'z') {
				number -= amount;
				if(number < (int)'a') {
					number = (int)'z'+1 - ((int)'a'-number);
				}
				newChar = String.valueOf((char)number);
			}
			else if((int)toArray[i]>=(int)'A'&& (int)toArray[i]<=(int)'Z') {
				number -= amount;
				if(number < (int)'A') {
					number = (int)'Z'+1 - ((int)'A'-number);
				}
				newChar = String.valueOf((char)number);				
			}
			else {
				newChar = ""+ toArray[i];
			}
			
			returnValue = returnValue + newChar;
		}
		return returnValue;
	}

	@Override
	public String getFounder(String founder) {
		// TODO Auto-generated method stub
		return null;
	}

}
