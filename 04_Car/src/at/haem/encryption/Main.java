package at.haem.encryption;

public class Main {
	public static void main(String[] args) {
		
		Manager m = new Manager();
		Encrypter c1 = new Cesar("Thom");
		Encrypter a1 = new Algo1("Dirk");
		Encrypter a2 = new Algo2();
		Encrypter c2 = new Cesar_24(2);
		/*
		m.setEncrypter(c1);
		String encryptetWord1 = m.encrypt("funktioniert 1");
		String decryptWord1 = m.decrypt(encryptetWord1);
		
		m.setEncrypter(a1);
		String encryptetWord2 = m.encrypt("funktioniert 2");
		String decryptWord2 = m.decrypt(encryptetWord2);
		
		m.setEncrypter(a2);
		String encryptetWord3 = m.encrypt("funktioniert 3");
		String decryptWord3 = m.decrypt(encryptetWord3);
			*/
		
		m.setEncrypter(c2);
		String encryptetWord4 = m.encrypt("Zebras yind cool");
		String decryptWord4 = m.decrypt(encryptetWord4);
	
		/*
		System.out.println(encryptetWord1);
		System.out.println(decryptWord1);
		System.out.println(encryptetWord2);
		System.out.println(decryptWord2);
		System.out.println(encryptetWord3);
		System.out.println(decryptWord3);
		*/
		System.out.println(encryptetWord4);
		System.out.println(decryptWord4);
		
	}
}
