package at.haem.encryption;

import java.util.ArrayList;
import java.util.List;

public class Manager implements Encrypter {
	public List<Encrypter> encrypters;
	public int setEncrypter;

	public Manager() {
		encrypters = new ArrayList<Encrypter>();
	}
	
	@Override
	public String encrypt(String text) {
		String newText = encrypters.get(setEncrypter).encrypt(text);
		return newText;
	}
	
	public void doEncrypt(String text) {
		
	}
	
	public void setEncrypter(Encrypter encrypter) {
		if(!encrypters.contains(encrypter)) {
			encrypters.add(encrypter);
		}
			this.setEncrypter = encrypters.indexOf(encrypter);
	}

	@Override
	public String getFounder(String founder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String decrypt(String text) {
		String decryptedText = encrypters.get(setEncrypter).decrypt(text);
		return decryptedText;
	}
}
