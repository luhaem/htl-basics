package at.haem.encryption;

import org.jasypt.util.text.StrongTextEncryptor;

public class Algo2 extends AbstractEncryptor{

	public String founder = "M�ller";
	StrongTextEncryptor textEncryptor = new StrongTextEncryptor();
	
	public Algo2() {
		super();
		this.founder = founder;
	}

	@Override
	public String encrypt(String text) {

		textEncryptor.setPassword("pw");
		String myEncryptedText = textEncryptor.encrypt(text);
		return myEncryptedText;
	}

	@Override
	public String decrypt(String text) {
		String plainText = textEncryptor.decrypt(text);
		return plainText;
	}
}
