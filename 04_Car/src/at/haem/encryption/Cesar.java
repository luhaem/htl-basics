package at.haem.encryption;

import org.jasypt.util.text.AES256TextEncryptor;

public class Cesar extends AbstractEncryptor{
	
	public String founder;
	AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
	public Cesar(String founder) {
		super();
		this.founder = founder;
	}

	@Override
	public String encrypt(String text) {

		textEncryptor.setPassword("pw");
		String myEncryptedText = textEncryptor.encrypt(text);
		return myEncryptedText;
	}

	@Override
	public String decrypt(String text) {
		String plainText = textEncryptor.decrypt(text);
		return plainText;
	}
}
