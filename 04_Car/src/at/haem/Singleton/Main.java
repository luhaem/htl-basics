package at.haem.Singleton;

public class Main {

	public static void main(String[] args) {
		
		Counter c1 = Counter.getInstance();
		Counter c2 = Counter.getInstance();
		Counter c3 = Counter.getInstance();
		c1.addOne();
		c2.addOne();
		c1.addOne();
	}
}
