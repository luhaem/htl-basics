package at.haem.Singleton;

public class Counter {

	private static Counter counter;
	private int count;

	private Counter() {
		count = 0;
	}

	public static Counter getInstance() {
		if (counter == null) {
			counter = new Counter();

		}
		return counter;
	}

	public void addOne() {
		count++;
		System.out.println(count);
	}
}
