package at.haem.bankomat;

public class bankomat {
	private float kontostand;
	private float maximalerÜberzug;
	private String KontoBesitzer;
	
	public bankomat(float kontostand, float höchsterÜberzug, String kontoBesitzer) {
		super();
		this.kontostand = kontostand;
		this.maximalerÜberzug = höchsterÜberzug;
		KontoBesitzer = kontoBesitzer;
	}

	public void einzahlen(float betrag) {
		if(betrag>0) {
			kontostand += betrag;
			System.out.println("Sie haben " + betrag +" eingezahlt");
		}
	}
	
	public void abheben(float betrag) {
		if(betrag <= kontostand + maximalerÜberzug) {
			kontostand-= betrag;
			System.out.println("Sie haben " + betrag + " abgehoben!");
		}
		else {
		System.out.println("Ihre Abbuchung ist ungültig. Eventuell haben Sie einen zu geringen Kontostand!");
		}
	}
	
	public void kontostandAnzeigen() {
		System.out.println("Ihr Kontostand beträgt " + kontostand);
	}
}
