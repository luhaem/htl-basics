package at.haem.ObserverPattern;

public interface Observable {
	
	public void inform();
	
}
