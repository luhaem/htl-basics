package at.haem.ObserverPattern;

import java.util.ArrayList;
import java.util.List;

public class Sensor implements Observable{

	public List<Observable> observables;
	
	public Sensor() {
		super();
		this.observables = new ArrayList<Observable>();
	}

	public void addObservable(Observable o1) {
		observables.add(o1);
	}

	public void informAll() {
		for (Observable observable : observables) {
			observable.inform();
		}
	}


	@Override
	public void inform() {
		// TODO Auto-generated method stub
		
	}
	
}
