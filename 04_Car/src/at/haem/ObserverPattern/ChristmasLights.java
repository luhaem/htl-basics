package at.haem.ObserverPattern;

public class ChristmasLights implements Observable{

	@Override
	public void inform() {
		System.out.println("Weihnachtsbeleuchtung leuchtet");
	}
}
