package at.haem.ObserverPattern;

public class Lantern implements Observable{

	@Override
	public void inform() {
		System.out.println("Laterne leuchtet!");
	}

}
