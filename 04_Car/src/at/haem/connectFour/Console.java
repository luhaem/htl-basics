package at.haem.connectFour;

import java.util.Scanner;

public class Console implements GUI{
	Scanner scanner;
	public Console() {
		 scanner = new Scanner(System.in);
	}
	
	@Override
	public void printOut(String printString) {
		System.out.println(printString);
		
	}

	@Override
	public int nextInput(String printString) {
		int input = scanner.nextInt();
		//System.out.println("Ihre Eingabe ist " + input);
		return input;
	}
	
	public String nextInputString(String printString) {
		String input = scanner.next();
		return input;
	}
	
}
