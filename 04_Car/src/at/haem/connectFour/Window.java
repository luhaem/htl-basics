package at.haem.connectFour;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.print.PrintService;
import javax.swing.*;

public class Window implements GUI{

	JButton btn1 = new JButton("Hallo1");
	JButton btn2 = new JButton("Hallo2");
	JButton btn3 = new JButton("Hallo3");
	JButton btn4 = new JButton("Hallo4");
	JButton btn5 = new JButton("Hallo5");
	JButton btn6 = new JButton("Hallo6");
	JButton btn7 = new JButton("Hallo7");
	JFrame f;
	JPanel p;

	public Window() throws IOException {
		
		createPanel();
		printOut("Hallo");
	}
	
	public void createPanel() throws IOException {
		f = new JFrame();
		p = new JPanel();
		p.setLayout(new GridLayout(7,7));
		for (int i = 0; i < 42; i++) {
			p.add(getImage());
		}
		p.add(btn1);
		p.add(btn2);
		p.add(btn3);
		p.add(btn4);
		p.add(btn5);
		p.add(btn6);
		p.add(btn7);
		f.add(p);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.pack();
		f.setVisible(true);
	}
	
	public JLabel getImage() throws IOException {
		//BufferedImage myPicture = ImageIO.read(new File("circle.png"));
		JLabel picLabel = new JLabel();
		ImageIcon imageIcon = new ImageIcon(new ImageIcon("circle.png").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
		picLabel.setIcon(imageIcon);
		return picLabel;
	}
		
	@Override
	public void printOut(String printString) {
		JOptionPane.showMessageDialog(null, printString);
	}

	@Override
	public int nextInput(String printString) {

		return 0;
	}

	@Override
	public String nextInputString(String printString) {
        String name = JOptionPane.showInputDialog(printString, null);
		return null;
	}

}
