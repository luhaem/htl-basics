package at.haem.connectFour;

public class Player {

	private String Playername;
	private int wins;
	private String symbol;
	
	public Player(String playername, String symbol) {
		super();
		Playername = playername;
		this.symbol = symbol;
	}
	
	public String getSymbol() {
		return this.symbol;	
	}
	
	public String getName() {
		return Playername;
	}
}