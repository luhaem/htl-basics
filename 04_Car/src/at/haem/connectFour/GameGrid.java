package at.haem.connectFour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameGrid{

	private String[][] grid;
	private List<Player> players;
	private int setPlayer;
	private Player currentPlayer;
	private int rowCount;
	private int winSum;
	private boolean underProgression;
	
	public GameGrid(int rowCount, int columnCount, int winSum) {
		super();
		grid = new String[rowCount][columnCount];
		players = new ArrayList<Player>();
		this.rowCount = rowCount;
		this.winSum = winSum;
		underProgression = true;
	}

	public void startGame(GUI selectedView) {
		
		selectedView.printOut("Willkommen zu vier gewinnt!");
		//selectedView.printOut("Wer m�chte starten?");
		//int eingabe = selectedView.nextInput("Wer m�chte starten?");
		currentPlayer = players.get(0);
		selectedView.printOut(currentPlayer.getName() + " startet! Viel Spa�");
		showGrid2();
		
		while(underProgression) {
				int nextInput = selectedView.nextInput("");
	        	push(nextInput-1, currentPlayer.getSymbol());
	        	checkWinner(currentPlayer);
	        	currentPlayer = changeCurrentPlayer();
	        	selectedView.printOut(currentPlayer.getName() + " ist an der Reihe");
	        	showGrid2();
        		}
		changeCurrentPlayer();
		selectedView.printOut("Spiel beendet! Sieger ist " + currentPlayer.getName());
	}
	
	public void checkWinner(Player player) {
		checkInDerReihe(player);
		checkInDerSpalte(player);
		checkQuerUnten1(player);
		checkQuerUnten2(player);
		checkQuerOben1(player);
		checkQuerOben2(player);
	}
	
	public void fillArray() {
		grid[0][0] = "X";
		grid[0][1] = "O";
		grid[0][2] = "O";
		grid[0][3] = "O";
		grid[1][0] = "X";
		grid[1][1] = "O";
		grid[1][2] = "O";
		grid[1][3] = "O";
		grid[2][0] = "X";
		grid[2][1] = "O";
		grid[2][2] = "O";
		grid[2][3] = "O";
		grid[2][4] = "O";
		grid[3][0] = "X";
		grid[3][1] = "O";
		grid[3][2] = "O";
		grid[3][3] = "O";
		grid[3][4] = "O";
		grid[3][5] = "O";
		grid[4][0] = "X";
		grid[4][1] = "O";
		grid[4][2] = "O";
		grid[4][3] = "O";
		grid[4][4] = "O";
		grid[4][5] = "O";
		grid[5][0] = "X";
		grid[5][1] = "O";
		grid[5][2] = "O";
		grid[5][3] = "O";
		grid[6][0] = "X";
		grid[6][1] = "O";
		grid[6][2] = "O";
		grid[6][3] = "O";
		grid[6][4] = "O";
		grid[6][5] = "O";
	}
	
	public void push(int column, String symbol) {
		for (int i = 0; i < grid[column].length; i++) {
			if(grid[column][i] == null) {
				grid[column][i] = symbol;
				i = grid[column].length;
			}
		}
	}
	
	public void showGrid2() {
		String print = "";
		for (int i = grid[0].length-1; i >= 0; i--) {
			for (int j = 0; j < grid.length; j++) {
				if(grid[j][i] == null) {
					print += "--" + " \t";
				}
				else {
					print += grid[j][i] + " \t"; 
				}
			}
			print += "\n";
		}
		System.out.println(print);
	}
	
	public void addPlayer(Player player) {

		if(!players.contains(player)) {
			players.add(player);
		}
	}
	
	public Player changeCurrentPlayer() {
		setPlayer= (setPlayer +1)%2;
		return players.get(setPlayer);
	}
	
	public int getRowCount() {
		return rowCount;
	}
	
	public void checkInDerReihe(Player player) {
		int count=0;
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				if(grid[i][j]==player.getSymbol()) {
					count+=1;
					if(count == winSum) {
						underProgression = false;
					}
				}
				else {
					count = 0;
				}
			}
		}
	}
	
	public void checkInDerSpalte(Player player) {
		int countRow=0;
		for (int j = 0; j < grid[j].length; j++) {
			for (int i = 0; i < grid.length; i++) {
				if(grid[i][j] == player.getSymbol()) {
					countRow+=1;
					if(countRow == winSum) {
						underProgression = false;
					}
				}
				else {
					countRow = 0;
				}
			}
			
		}
	}
	
	public void checkQuerUnten1(Player player) {
		
		int countRow=0;
		int countColumn=0;
		int toWin=1;
		boolean exit=false;
		
		for (int i = 0; i < grid.length; i++) {
			exit=false;
			countRow = i;
			countColumn = 0;
			toWin = 1;
			
			while(grid[countRow][countColumn] != null && exit==false) {
			
			if(countRow < grid.length-1 && countColumn < grid[countRow].length-1) {
			
				if(grid[countRow][countColumn] == player.getSymbol()) {
					
				if(grid[countRow+1][countColumn+1] == player.getSymbol()) {
					toWin +=1;
					if(toWin==winSum) {
						underProgression = false;
						System.out.println("Das letzte Feld ist :" + countRow + " und " + countColumn);
					}
				}
				countRow++;
				countColumn++;
			}
			else {
				countRow++;
				countColumn++;
				toWin=1;
			}
			}
			else {
				exit = true;
			}
			}
		}
	}
	
	public void checkQuerUnten2(Player player) {
		int countRow=0;
		int countColumn=0;
		int toWin=1;
		boolean exit;
		
		for (int i = 0; i < grid[i].length; i++) {
			exit=false;
			countRow = 0;
			countColumn = i;
			toWin = 1;
			while(grid[countRow][countColumn] != null && exit == false) {
				
			if(countRow < grid.length-1 && countColumn < grid[countRow].length-1) {
				
			if(grid[countRow][countColumn] == player.getSymbol()) {
				if( grid[countRow+1][countColumn+1] == player.getSymbol()) {
					toWin +=1;
					if(toWin==winSum) {
						underProgression = false;
						System.out.println("Das letzte Feld der Reihe ist " + countRow + " und " + countColumn);
					}
					}
				countRow++;
				countColumn++;
			}
			else {
				countRow++;
				countColumn++;
				toWin=1;
			}
			}else {
				exit= true;
			}
			}
		}
	}

	public void checkQuerOben1(Player player) {
		int countRow=0;
		int countColumn=0;
		int toWin=1;
		boolean exit;
		
		for (int i = grid.length-1; i > 0; i--) {
			countRow = i;
			countColumn=0;
			exit = false;
			toWin = 1;
			
			while(grid[countRow][countColumn] != null && exit == false) {
				
				if(countRow > 0 && countColumn < grid[countRow].length-1) {
					if(grid[countRow][countColumn] == player.getSymbol()) {
						if(grid[countRow-1][countColumn+1] == player.getSymbol()) {
								toWin +=1;
							if(toWin==winSum) {
								underProgression = false;
								System.out.println("Das letzte Feld der Reihe ist " + countRow + " und " + countColumn);
							}
						}
					countRow--;
					countColumn++;
					}
					else {
						countRow--;
						countColumn++;
						toWin=1;
					}
				}
				else {
					exit = true;
				}
			}
		}
	}
	
	public void checkQuerOben2(Player player) {
		int countRow=0;
		int countColumn=0;
		int toWin=1;
		boolean exit = false;
		
		for (int i = 0; i < grid[i].length-1; i++) {
			countRow = grid.length-1;
			countColumn=i;
			exit = false;
			toWin = 1;
			while(grid[countRow][countColumn] != null && exit == false) {
				
			
			if(countRow > 0 && countColumn < grid[countRow].length-1) {
				
			if(grid[countRow][countColumn] == player.getSymbol()) {
				if( grid[countRow-1][countColumn+1] == player.getSymbol()) {
					toWin +=1;
					if(toWin==winSum) {
						underProgression = false;
						System.out.println("Das letzte Feld der Reihe ist " + countRow + " und " + countColumn);
					}
					}
				countRow--;
				countColumn++;
			}
			else {
				countRow--;
				countColumn++;
				toWin=1;
			}
			}
			else {
				exit = true;
				}
			}
		}
	}
	
	public boolean isUnderProgression() {
		return underProgression;
	}

	public void setUnderProgression(boolean underProgression) {
		this.underProgression = underProgression;
	}


}

