package at.haem.connectFour;

public interface GUI {

	public void printOut(String printString);
	public int nextInput(String printString);
	public String nextInputString(String printString);
}
