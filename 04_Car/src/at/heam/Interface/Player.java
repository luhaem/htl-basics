package at.heam.Interface;

import java.util.ArrayList;
import java.util.List;

public class Player implements Playable{

	private List<Playable> playables;
	
	public Player() {
		playables = new ArrayList<Playable>();
	}

	@Override
	public void play() {
		// TODO Auto-generated method stub
	}
	
	public void addPlayable(Playable p) {
		playables.add(p);
	}
	
	public void playAll() {
		for (Playable playable : playables) {
			playable.play();
		}
	}
}
