package at.heam.Interface;

public class Main {
	public static void main(String[] args) {
		
	Playable Item = new Item();
	Playable Title = new Title();
	Playable Song = new Song();
	
	Player Player = new Player();
	
	Player.addPlayable(Item);
	Player.addPlayable(Title);
	Player.addPlayable(Song);
	
	Player.playAll();
	Song.play();

	}
}
