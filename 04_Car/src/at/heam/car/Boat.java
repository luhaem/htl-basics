package at.heam.car;

import java.awt.Color;

public class Boat extends Vehicle {
	private String propeller;

	public Boat(String name, Color color, String propeller, int price, int highspeed) {
		super(name, color, price, highspeed);
		this.propeller = propeller;
	}
	/**
	 * @return the propeller
	 */
	public String getPropeller() {
		return propeller;
	}
	/**
	 * @param propeller the propeller to set
	 */
	public void setPropeller(String propeller) {
		this.propeller = propeller;
	}
}
