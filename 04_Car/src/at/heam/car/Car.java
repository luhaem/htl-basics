package at.heam.car;

import java.awt.Color;

public class Car extends Vehicle {
	private int amountOfTyres;
	private int discount;

	public Car(String name, Color color, int amountOfTyres, int price, int discount, int highspeed) {
		super(name, color, price, highspeed);
		this.amountOfTyres = amountOfTyres;
		this.discount = discount;
	}

	public void doBlink() {
		System.out.println("blink blink");
	}
	/**
	 * @return the amountOfTyres
	 */
	public int getAmountOfTyres() {
		return amountOfTyres;
	}
	/**
	 * @param amountOfTyres the amountOfTyres to set
	 */
	public void setAmountOfTyres(int amountOfTyres) {
		this.amountOfTyres = amountOfTyres;
	}
	@Override
	public double getPrice() {
		double percent = 0.01 * this.discount;
		return price*(1- percent);
	}
}
