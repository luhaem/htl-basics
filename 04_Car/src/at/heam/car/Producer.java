package at.heam.car;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Producer {
	private String name;
	private List<Vehicle> vehicles;

	public Producer(String name) {
		super();
		this.vehicles = new ArrayList();
		//LinkedList auch m�glich
		this.name = name;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public double getValueOfVehicles() {
		double value =0;
		for(Vehicle veh: vehicles){
			value += veh.getPrice();
		}
		return value;
	}
	public void addVehicle(Vehicle v) {
		vehicles.add(v);
	}
}
