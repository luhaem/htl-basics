package at.heam.car;

import java.awt.Color;

public class Main {
	
	public static void main(String[] args) {
		
		Boat boat = new Boat("Speedi", Color.black, "propeller1", 10000, 100);
		Car car = new Car("Formel1", Color.green, 4, 5000, 5, 220);
		Producer producer = new Producer("Tesla");
		Vehicle Porsche = new Car("Porsche 911", Color.BLACK, 4, 1000, 10, 30);
		
		System.out.println(car.getColor());
		
		
		car.doBlink();
		
		
		System.out.println(Porsche.getPrice());
		
		producer.addVehicle(Porsche);
		producer.addVehicle(boat);
		System.out.println(producer.getValueOfVehicles());	
		
		Porsche.gas(50);
		
	}
}
