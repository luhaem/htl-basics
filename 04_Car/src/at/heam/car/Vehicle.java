package at.heam.car;

import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;

public class Vehicle {
	private String name;
	private Color color;
	protected int price;
	private int speed;
	private int highspeed;
	
	public Vehicle(String name, Color color, int price, int highspeed) {
		super();
		this.name = name;
		this.color = color;
		this.price = price;
		this.highspeed = highspeed;
	}

	public void gas(int power) {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				System.out.println(speed + "km/h");
				if(speed <= highspeed) {
				speed+= power*0.1;
				}
			}
		}, 1000, 1000);
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}
}
