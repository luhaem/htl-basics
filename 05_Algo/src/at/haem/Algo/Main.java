package at.haem.Algo;

public class Main {
	
	public static void main(String[] args) {
		
		int[] intArray = new int[] {5,2,8,1,7};
		
		Algorithm iS = new InsertionSort();
		Algorithm sS = new SelectionSort();
		Algorithm bS = new BubbleSort();
		
		Sorter sorter = new Sorter();
		sorter.setAlgorithm(bS);
		
		sorter.doSort(sorter.getRandomArray());
	}
}
