package at.haem.Algo;

public class InsertionSort implements Algorithm{

	@Override
	public int[] doSort(int[] unsortedArray) {
		int temp;
		for (int i = 1; i < unsortedArray.length; i++) {
			temp = unsortedArray[i];
			int j = i;
			while (j > 0 && unsortedArray[j - 1] > temp) {
				unsortedArray[j] = unsortedArray[j - 1];
				j--;
			}
			unsortedArray[j] = temp;
		}
		return unsortedArray;
	}
}
