package at.haem.Algo;

import java.util.Arrays;

public class Sorter {

	private Algorithm SelectedAlgo;
	
	public Sorter() {
		super();
	}

	public int[] getRandomArray() {
		//noch nicht fertig
	    int[] list = new int[10];
	    for (int i=0; i<10; i++){
	        int n = (int)(Math.random()*9 + 1);
	        list[i] = n;
	   	}
	    return list;
	   }
	
	public void setAlgorithm(Algorithm algo) {
		this.SelectedAlgo = algo;
	}
	
	public int[] doSort(int[] unsortedArray) {
		
		System.out.println("altes Array" + Arrays.toString(unsortedArray));	
		
		int[] sortedArray = SelectedAlgo.doSort(unsortedArray);
		
		System.out.println("neues Array" + Arrays.toString(sortedArray));
		
		return sortedArray;
	}
}
