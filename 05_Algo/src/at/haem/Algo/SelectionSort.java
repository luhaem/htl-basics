package at.haem.Algo;

public class SelectionSort implements Algorithm{

	@Override
	public int[] doSort(int[] unsortedArray) {
		
        for (int i = 0; i < unsortedArray.length - 1; i++)  
        {  
            int index = i;  
            for (int j = i + 1; j < unsortedArray.length; j++){  
                if (unsortedArray[j] < unsortedArray[index]){  
                    index = j;//searching for lowest index  
                }  
            }  
            int smallerNumber = unsortedArray[index];   
            unsortedArray[index] = unsortedArray[i];  
            unsortedArray[i] = smallerNumber;  
        }  
        return unsortedArray;
	}

}
