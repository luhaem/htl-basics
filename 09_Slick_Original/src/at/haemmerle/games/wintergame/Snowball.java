package at.haemmerle.games.wintergame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Snowball {
	
	private double x,y;
	private double size;
	
	public Snowball(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.size = 20;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	public void update(GameContainer gc, int delta) {
		this.x += 1;
	}
	
	public void render(Graphics graphics) {
		graphics.setColor(Color.white);
		graphics.fillOval((float) this.x, (float)this.y, (float)this.size, (float)this.size);
	}
}

