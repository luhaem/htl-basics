package at.haemmerle.games.wintergame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Snowman {
	
	private double x,y;
	private double size;
	
	public Snowman(double x, double y) {
		super();
		this.x = x;
		this.y = y;
		this.size = 20;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyPressed(Input.KEY_UP) && this.y > 0) {
			this.y -= 25;
		}
		if (gc.getInput().isKeyPressed(Input.KEY_DOWN)&& this.y < 520) {
			this.y += 25;
		}
	}
	
	public void render(Graphics graphics) {
		graphics.setColor(Color.white);
		graphics.fillOval((float) this.x, (float)this.y, (float)this.size, (float)this.size);
		graphics.fillOval((float) this.x-5, (float)this.y+15, (float)this.size+10, (float)this.size+10);
		graphics.fillOval((float) this.x-15, (float)this.y+30, (float)this.size+30, (float)this.size+30);
	}
}

