package at.haemmerle.games.wintergame;

import org.newdawn.slick.Graphics;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;

public class Meteoroid {
	
	private double x,y;
	private double speedx, speedy;
	private float size;
	private Random r;
	private int timeToWait;


	public Meteoroid(int size) {
		super();
		this.size = size;
		this.r = new Random();
		this.y = 0;
		this.x = r.nextInt(400);
		this.speedx = r.nextInt(10)/10 + 0.8;
		this.speedy = r.nextInt(10)/10 + 0.8;	
		this.timeToWait = r.nextInt(4000)+1000;
	}

	public void update(GameContainer gc, int delta) {
		
		this.x += speedx;
		this.y += speedy;
		
		if (this.y > 0) {
			if(this.size > 0.1) {
				this.size -= 0.02*delta;
				}
		    }
		
		if(this.y >= timeToWait) {
			this.timeToWait = r.nextInt(2000)+1000;
			this.x = r.nextInt(400);
			this.y = 0;
			this.speedx = r.nextInt(10)/10 + 0.8;
			this.speedy = r.nextInt(10)/10 + 0.8;
			this.size = 20;
		}
	}
	
	public void render(Graphics graphics) {

		graphics.setColor(Color.yellow);
		graphics.fillOval((float) this.x, (float)this.y, this.size, this.size);

	}
}
