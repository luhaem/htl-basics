package at.haemmerle.games.wintergame;

import org.newdawn.slick.Graphics;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;

public class Snowflake {
	
	private double x,y;
	private double speed;
	private int size;
	private Random r;


	
	public Snowflake(double speed, int size) {
		super();
		this.speed = speed;
		this.size = size;
		this.r = new Random();
		this.y = -(r.nextInt(600));
		this.x = r.nextInt(800);
	}
	

	public void update(GameContainer gc, int delta) {
		this.y += speed;
		if(this.y >= 600) {
			this.x = r.nextInt(800);
			this.y = 0;
		}
	}
	
	public void render(Graphics graphics) {

		graphics.setColor(Color.white);
		graphics.fillOval((float) this.x, (float)this.y, this.size, this.size);

	}
}
