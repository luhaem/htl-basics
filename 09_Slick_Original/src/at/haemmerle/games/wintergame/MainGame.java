package at.haemmerle.games.wintergame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.tests.AnimationTest;

public class MainGame extends BasicGame {

	private Meteoroid meteroid;
	private Snowman snowman;
	private List<Snowflake> snowflakes = new ArrayList<Snowflake>();
	private List<Snowball> snowballs = new ArrayList<Snowball>();

	public MainGame(String title) {
		super(title);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// es wird gezeichnet
		meteroid.render(graphics);
		snowman.render(graphics);
		
		for (Snowflake sf : snowflakes) {
			sf.render(graphics);
		}
		for (Snowball sb : snowballs) {
			sb.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// einmal aufgerufen
		meteroid = new Meteoroid(20);
		snowman = new Snowman(20, 300);
		
		for (int i = 0; i <= 50; i++) {
			Snowflake snowflakeSmall = new Snowflake(0.2, 4);
			Snowflake snowflakeMedium = new Snowflake(0.4, 6);
			Snowflake snowflakeLarge = new Snowflake(0.6, 10);
			snowflakes.add(snowflakeSmall);
			snowflakes.add(snowflakeMedium);
			snowflakes.add(snowflakeLarge);
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta= Zeit seit letztem Aufruf

		meteroid.update(gc, delta);
		snowman.update(gc, delta);

		if (gc.getInput().isKeyPressed(Input.KEY_SPACE)) {
			Snowball snowball = new Snowball(15,snowman.getY()+25);
			snowballs.add(snowball);
		}
			
		for (Snowflake sf : snowflakes) {
			sf.update(gc, delta);
		}
		
		for (Snowball sb : snowballs) {
			sb.update(gc, delta);
		}
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}

